<?php
/**
 * @file
 * basis_search.features.inc
 */

/**
 * Implements hook_default_search_api_server().
 */
function basis_search_default_search_api_server() {
  $items = array();
  $items['default'] = entity_import('search_api_server', '{
    "name" : "Default",
    "machine_name" : "default",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "2",
      "partial_matches" : 1
    },
    "enabled" : "1"
  }');
  return $items;
}
